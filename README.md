# Awesome

## Best Practices

- https://hackernoon.com/optimising-the-front-end-for-the-browser-f2f51a29c572#.1j1h5x44h

## CSS

- http://iamvdo.me/en/blog/css-font-metrics-line-height-and-vertical-align?utm_source=CSS-Weekly&utm_campaign=Issue-253&utm_medium=email
- http://sass-lang.com/
- http://webdesignerwall.com/tutorials/level-css-skills-20-pro-css-tips
- https://bitsofco.de/6-reasons-to-start-using-flexbox/
- https://bitsofco.de/linting-html-using-css/
- https://blog.gyrosco.pe/smooth-css-animations-7d8ffc2c1d29#.dzcq5vuu0
- https://css-tricks.com/the-blur-up-technique-for-loading-background-images/
- https://github.com/raphamorim/origami.js
- https://jmperezperez.com/medium-image-progressive-loading-placeholder/
- https://jonsuh.com/hamburgers/
- https://kyusuf.com/post/a-couple-of-alternatives-to-the-hamburger-menu
- https://kyusuf.com/post/almost-complete-guide-to-flexbox-without-flexbox
- https://medium.freecodecamp.com/an-animated-guide-to-flexbox-d280cf6afc35#.qz4dbunhy
- https://webdesign.tutsplus.com/tutorials/revisiting-the-css-background-property--cms-25991

## Colors

- https://medium.com/@JustinMezzell/how-i-work-with-color-8439c98ae5ed
- https://medium.com/eightshapes-llc/color-in-design-systems-a1c80f65fa3

## CDN

- https://medium.freecodecamp.com/an-illustrated-guide-for-setting-up-your-website-using-github-cloudflare-5a7a11ca9465

## Fonts

- https://www.awwwards.com/20-best-web-fonts-from-google-web-fonts-and-font-face.html

## HTML

- https://cloudfour.com/thinks/autofill-what-web-devs-should-know-but-dont/
- https://formlinter.com/
- https://github.com/joshbuchea/HEAD/blob/master/README.md
- https://html5up.net/
- https://www.youtube.com/watch?v=CZGqnp06DnI

## Images

- http://thenewcode.com/28/Making-And-Deploying-SVG-Favicons
- https://www.invisionapp.com/blog/7-tips-for-designing-awesome-gifs/

## README

- http://engineering.khanacademy.org/posts/time-management-multiple-authors.htm
- http://hiring.readme.io/2342-carla-walton
- http://www.humanops.com/ 
- https://amasad.me/perfectionism
- https://frank.taillandier.me/2016/03/08/les-gestionnaires-de-contenu-statique/
- https://getpocket.com/explore/item/how-i-became-a-morning-person-and-why-i-decided-to-make-the-change-1350318180
- https://medium.com/firm-narrative/want-a-better-pitch-watch-this-328b95c2fd0b
- https://medium.com/france/les-d%C3%A9veloppeurs-sont-devenus-des-cols-bleus-457bd6287631
- https://medium.com/the-mission/impostor-syndrome-5-reasons-you-should-have-it-ae5bf0ffdea1#.xqx5w1tgq
- https://medium.com/the-mission/iron-man-and-the-modern-identity-crisis-45a6919b3e76
- https://www.inc.com/jeff-haden/7-things-steve-jobs-said-that-you-should-say-every-single-day.html
- https://www.ted.com/talks/yves_morieux_how_too_many_rules_at_work_keep_you_from_getting_things_done

## Sketch

- https://github.com/teracyhq/bootflat-ui-kit-sketch
- https://medium.com/sketch-app-sources/how-to-be-more-organized-while-designing-ui-90d2d69cfb4f

## UX

- http://joelcalifa.com/blog/patronizing-passwords/
- https://baremetrics.com/blog/gifs-feature-rollout
- https://kyusuf.com/post/a-couple-of-alternatives-to-the-hamburger-menu
- https://medium.com/swlh/creative-form-input-field-design-examples-bfe5dd50808a
- https://www.smashingmagazine.com/2016/09/how-to-design-error-states-for-mobile-apps/